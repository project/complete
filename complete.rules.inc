<?php

/**
 * @file
 * Rules integration for the complete module
 */

/**
 * Implementation of hook_rules_condition_info().
 * 
 * @ingroup rules
 * @see content_complete_completeness_percentage_form
 */
function complete_rules_condition_info() {
  return array(
    'complete_action_is_incomplete' => array(
      'label' => t('Is incomplete'),
      'module' => 'Complete',
    ),
  );
}

/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function complete_rules_action_info() {
  return array(
    'complete_action_complete' => array(
      'label' => t('Set completed'),
      'module' => 'Complete',
    ),
  );
}

/**
 * Condition form
 * 
 * TODO: remove if we find out about http://drupal.org/node/483054
 */
function complete_action_is_incomplete_form($settings = array(), &$form) {
	// this is a dirty trick to save the rule we are currently adding condition/action for
	$rule_name = arg(3);
			
  $form['settings']['rule_name'] = array(
    '#type' => 'hidden',
    '#title' => t('Rule name'),
    '#default_value' => $rule_name,
    '#description' => t('Give the rule name to associate it with. Will be removed soon!')
  );
}

/**
 * Action form
 * 
 * TODO: remove if we find out about http://drupal.org/node/483054
 */
function complete_action_complete_form($settings = array(), &$form) {
	$rule_name = arg(3);
		
  $form['settings']['rule_name'] = array(
    '#type' => 'hidden',
    '#title' => t('Rule name'),
    '#default_value' => $rule_name,
    '#description' => t('Give the rule name to associate it with. Will be removed soon!')
  );
}


function complete_action_is_incomplete($settings, $element, $state) {
  $rule = $settings['rule_name'];  
  $nodes = complete_get_complete_nodes($rule);
  
  // TODO: the rule part has to be rewritten, associating a rule with a node (from the node panel, not from within rules!)
  // we can still have this condition but it won't be necessary anymore
	foreach ($nodes as &$node) {
	  return $node->complete['runs'] < $node->complete['total_runs'];
	}
	
	// nothing found
	return FALSE;
}

// this is executed when the rule is fired (and the conditions are passed)
function complete_action_complete($settings, $element, $state) {  
  $rule = $settings['rule_name'];
	$nodes = complete_get_complete_nodes($rule);
	
	foreach ($nodes as &$node) {
	  $node->complete['runs']++;	  
	  _complete_update($node);
	}
}
