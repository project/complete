<?php

/**
 * @file Complete module
 */

define('COMPLETE_NO_RULE_SELECTED', 'none');

/**
 * Implementation of hook_perm()
 */
function complete_perm() {
  return array(
    'administer complete', 
    'access complete',
  );
}

/**
 * Implementation of hook_menu()
 */
function complete_menu() {
  // Put the Complete menu callbacks in the Rules menu.
  $complete_admin_path = RULES_ADMIN_PATH .'/complete';
  
  $items[$complete_admin_path] = array(
    'title' => t('Complete'),
    'description' => t('Tag rules as required for completion'),
    'page callback' => 'drupal_get_form', 
    'page arguments' => array('complete_admin_settings'),
    'access arguments' => array('administer complete'), 
    'file' => 'complete.admin.inc',
    'type' => MENU_NORMAL_ITEM,    
  );
  
  $items[$complete_admin_path .'/settings'] = array(
    'title' => t('Settings'), 
    'description' => t('Tag rules as required for completion'), 
    'page callback' => 'drupal_get_form', 
    'page arguments' => array('complete_admin_settings'),
    'access arguments' => array('administer complete'), 
    'file' => 'complete.admin.inc',
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  
  $items[$complete_admin_path .'/reset'] = array(
    'title' => t('Reset counters'), 
    'description' => t('Reset complete counters'), 
    'page callback' => 'drupal_get_form',
    'page arguments' => array('complete_admin_reset_counters'), 
    'access arguments' => array('administer complete'), 
    'file' => 'complete.admin.inc',
    'weight' => -9,
    'type' => MENU_LOCAL_TASK,
  );
  
  return $items;
}

/**
 * Implementation of hook_block()
 * 
 * TODO: to be replaced with a views plugin, for more flexibility.
 */
function complete_block($op = 'list', $delta = 0, $edit = array()) {  
  if (user_access('access complete')) {
    switch ($op) {
      case 'list':
        $blocks[0]['info'] = t('Complete Percentage');
        $blocks[0]['cache'] = BLOCK_NO_CACHE;
        return $blocks;
        break;
      case 'view':
        $data = complete_get_data();                      
        $block['subject'] = t('Complete');          
        if (! empty($data) && !variable_get('complete_hide_block', FALSE) && $data['total']) {  
          $block['content'] = theme('complete_percent', $data);
        }
        return $block;
        break;
      case 'configure':
        $form['complete_hide_block'] = array(
          '#title' => t('Hide the blocks when the percentage reaches 100%'),
          '#type' => 'checkbox',
          '#default_value' => variable_get('complete_hide_complete', FALSE),
          '#description' => t('If you enable this feature, the blocks will be hidden when 100% has been reached.'),
        );
        return $form;
      case 'save':
        variable_set('complete_hide_block', $edit['complete_hide_block']);
        return;
    }
  }
}

/**
 * Return the rules
 * 
 * @param $select all, completed, uncompleted 
 */
function complete_get_rules($select = 'all') {
  $rules = rules_get_configured_items('rules');
  $result = array();
  foreach ($rules as $name => $rule) {
    if ($rule['#active']) { // only consider active rules
      $result[$name] = $rule;
    } 
  }
  return $result;
}

/**
 * Return the complete actions by querying the database, 
 * without the nodes.
 */
function complete_get_complete_actions() {  
  $rules = complete_get_rules();
  
  $result = db_query("SELECT * FROM {complete} ORDER BY nid ASC");
  $data = array();
  while ($item = db_fetch_array($result)) {
    // Attach the rule data.
    $item['#rule'] = $rules[$item['rule']];
    $data[] = $item;
  }
  return $data;
}

/**
 * Retrieves an array of nodes of the complete type,
 * which will contain the complete data in the node.
 */
function complete_get_complete_nodes($rule) {
  $result = db_query("SELECT nid FROM {complete} WHERE rule = '%s'", $rule);
  $nodes = array();
  while ($data = db_fetch_array($result)) {
    $nodes[] = node_load($data['nid']);
  }
  return $nodes;
}

/**
 * Save the complete action to the database.
 */
function complete_insert_action($action) {
  _complete_build_action($action);  
  drupal_write_record('complete', $action);
}

/**
 * Prepare the action.
 */
function _complete_build_action(&$action) {
  if (!isset($action['runs'])) $action['runs'] = 0;
  
  $action['completed'] = ($action['runs'] >= $action['total_runs']) ? 1 : 0;
  
  // Set the remaining number of runs, set to 0 if negative
  $remaining = $action['total_runs'] - $action['runs'];
  $action['remaining'] = ($remaining >= 0) ? $remaining : '0';
  
  // Do not allow more runs than total_runs
  $action['runs'] = ($action['runs'] > $action['total_runs']) ? $action['total_runs'] : $action['runs'];
} 

/**
 * Get the complete percentage data for all the rules.
 */
function complete_get_data() {
  $complete_data = array();
  
  $actions = complete_get_complete_actions();
    
  $percent = 0;
  $complete = 0;
  $total = 0;
  $subtask_percentages = array();
  
  $first = TRUE;
  foreach ($actions as $key => $value) {
    // Annotate this task with additional subtask percentages    
    complete_annotate_percentage($value);
        
    $total += $value['total_runs'];
    $complete += $value['runs'];
    $completed = $value['completed'];
    
    // Count subtask percentages if this action is not yet completed
    if (!$value['completed']) {
      $subtask_percentages[] = ($value['total_subtasks']) ? ($value['subtasks'] / $value['total_subtasks']) * 100 : 0;
    }
            
    if (!$completed && $first) {
      // add the next action
      $complete_data['nextaction'] = $value['#rule']['#label'];
      $complete_data['nextactionpath'] = $value['path'];
      $complete_data['nextpercent'] = 1; // a single action
      $complete_data['next'] = $value;
      
      $first = FALSE;
    }
  }

  $subtask_percent = 0;
  $percent = 0;
  
  if ($total) {
    $dec = number_format(($complete / $total), 2);
    $percent = $dec * 100;
    
    // Add subtask percentages if applicable
    foreach($subtask_percentages as $subtask_percentage) {
      // Normalize against number of other tasks
      $subtask_percent += floor($subtask_percentage * (1 / $total));
    }
  }
  
  $incomplete = $total - $complete;

  $complete_data['percent'] = $percent + $subtask_percent;
  $complete_data['completed'] = $complete;
  $complete_data['incomplete'] = $incomplete;
  $complete_data['total'] = $total;
  $complete_data['nextpercent'] = ($total != 0) ? $percent + (number_format(($complete_data['nextpercent'] / $total),2) * 100) : 0;
  $complete_data['complete_page_path'] = variable_get('complete_page_path', 'complete');
  $complete_data['complete_page_title'] = variable_get('complete_page_title', t('See all'));
      
  return $complete_data;
}

/**
 * Annotate an individual percentage for the subtasks involved
 * in the task. 
 * 
 * TODO: this has to be refactored in the Views API
 * 
 * WARNING: $value can be an object (from a view) or an array
 * (from the database). Accomodate both cases in the same way.
 */
function complete_annotate_percentage(&$value) {
  if (is_object($value)) {
    $path = $value->complete_path;
  } else {
    $path = $value['path'];
  }
  
  // If Content Complete is enabled & path is towards a node-edit, 
  // read in fine-grained percentage (field-based instead of
  // action-based).  
  $path_split = split("/", $path);
  if (module_exists('content_complete') && isset($path_split[1]) && is_numeric($path_split[1])) {
    $nid = $path_split[1];
    $node = node_load($nid);
    
    $cc = content_complete_get_data($node);
        
    // Convert Content Complete logic to Complete logic, stored as 'subtasks',
    // as parts of 'tasks' (now still called 'runs').
    // $total_subtasks = $cc['total'];
    // $subtasks = $cc['completed'];
    $annotations['total_subtasks'] = $cc['total'];
    $annotations['subtasks'] = $cc['completed'];
  
    // If Content Complete senses incompleteness, we revert the 
    // completeness of Complete, regardless of whether the 'action'
    // has been executed before.
    if($annotations['subtasks'] < $annotations['total_subtasks']) {
      $annotations['runs'] = 0;
      $annotations['completed'] = 0;
      $annotations['remaining'] = 1;
    }
        
    // Save changes back to array or object
    foreach($annotations as $field => $val) {
      if (is_array($value)) {
        $value[$field] = $val;
      } else {
        $member = 'complete_' .$field;
        $value->$member = $val;
      }
    }
  }  
}

/**
 * Reset the counters for all the complete actions.
 */
function complete_reset_counters() {
  $actions = complete_get_complete_actions();
  
  foreach ($actions as $action) {
    $action['runs'] = 0;
    _complete_update_action($action);
  }
}

/**
 * Implementation of hook_form_alter().
 * 
 * Intercept node edit form of complete-enabled types to store 
 * complete-related information (action path, number of runs, etc.)
 */
function complete_form_alter(&$form, $form_state, $form_id) {
  if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] .'_node_form' == $form_id && complete_enabled_type($form['type']['#value'])) {

    // get all rules
    $rules = complete_get_rules('all');
  	    
    $form['title']['#required'] = FALSE;
    $form['title']['#description'] = t('This field will be populated with the rule title. You can override by filling in this field.');
    
    $form['complete'] = array(
      '#title' => t('Complete Action'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE, 
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    
    $selection[COMPLETE_NO_RULE_SELECTED] = t('- None -');
    foreach ($rules as $name => $rule) {
      $selection[$name] = t($rule['#label']. " (" .$name. ")");
    }
        
    $form['complete']['rule'] = array(
      '#type' => 'select',
      '#title' => t('Rule'),
      '#description' => t('Choose a rule that triggers this action'),
      '#options' => $selection,
      '#default_value' => isset($form['#node']->complete['rule']) ? $form['#node']->complete['rule'] : COMPLETE_NO_RULE_SELECTED,
    );
    
    $form['complete']['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#description' => t('The path to redict the user to so he can complete the rule. Example: <code>content/add/page</code>.'),
      '#default_value' => isset($form['#node']->complete['path']) ? $form['#node']->complete['path'] : '',
		);
    
    $form['complete']['total_runs'] = array(
      '#type' => 'textfield',
      '#title' => t('Total runs'),
			'#description' => t('The number of times this rule has to be run before completion.'),
			'#size' => 3,
      '#default_value' => isset($form['#node']->complete['total_runs']) ? $form['#node']->complete['total_runs'] : '',
		);
		
		// TODO: add option to clear number of runs for this specific action, or even to change the number of times this rule has run (admin only!)
    
    $form['#validate'][] = 'complets_node_validate';
  }

}

/**
 * Add automatic node title for a complete node on validate.
 */
function complets_node_validate($form, &$form_state) {  
  if (empty($form_state['values']['title']) && isset($form_state['values']['complete']['rule'])) {
    $rules = complete_get_rules('all'); // make static variable?

    $rule = $rules[$form_state['values']['complete']['rule']];
    $label = $rule['#label'];
    
    form_set_value($form['title'], $label, $form_state);
  }
}

/**
 * Implementation of hook_nodeapi().
 * 
 * Insert/update complete values for complete enabled nodes.
 * Complete data will be attached to those nodes.
 */
function complete_nodeapi(&$node, $op, $teaser, $page) {
  if (isset($node->complete) || complete_enabled_type($node->type)) {
    switch ($op) {
      case 'insert':
        _complete_insert($node);
        break;
      case 'update':
        _complete_update($node);
        break;
      case 'load':
        if ($complete = db_fetch_array(db_query('SELECT * FROM {complete} WHERE nid = %d', $node->nid))) {
          $node->complete = $complete;          
        }
        break;
      case 'delete':
        _complete_delete($node);
        break;
    }
  }
}


/**
 * Implementation of hook_form_alter().
 * 
 * Intercept node type edit form to enable/disable complete node types.
 */
function complete_form_node_type_form_alter(&$form, $form_state) {
  $form['complete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Complete'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 35,
  );
  $form['complete']['complete_use'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use this content type as a complete type to store actions'),
    '#default_value' => variable_get('complete_use_'. $form['#node_type']->type, FALSE),
  );
}

/**
 * Determine if a given node type is a complete node type.
 * 
 * @param $type
 *   The node object or the node's type
 */
function complete_enabled_type($type) {
  if (is_object($type)) {
    $type = $type->type;
  }
  return variable_get('complete_use_'. $type, FALSE);
}

/**
 * Determine if a given node is a complete node.
 */
function complete_enabled_node(&$node) {  
  return isset($node->complete['rule']) && $node->complete['rule'] !== COMPLETE_NO_RULE_SELECTED;
}

/**
 * Implementation of hook_node_type().
 * Rename or delete the settings variable if a type changes.
 */
function complete_node_type($op, $info) {
  switch ($op) {
    case 'delete':
      variable_del('complete_use_'. $info->type);
      break;
    case 'update':
      if (!empty($info->old_type) && $info->old_type != $info->type) {
        if (complete_enabled_type($info->old_type)) {
          variable_del('complete_use_'. $info->old_type);
          variable_set('complete_use_'. $info->type, 1);
        }
      }
      break;
  }
}

/**
 * Insert complete data to the DB
 */
function _complete_insert($node) {
  if (complete_enabled_node($node)) {  
    // build the complete data
    $node->complete['nid'] = $node->nid;
  
    // insert in the database
    _complete_insert_action($node->complete);
  }
}

/**
 * Update complete data of an existing complete action
 */
function _complete_update(&$node) {  
  $old_config = node_load($node->nid);
  
  if (complete_enabled_node($node)) {
    // In that case this node has never had complete data. Should be created then, this is not really an update.
    if (!complete_enabled_node($old_config)) {
      _complete_insert($node, $teaser, $page);
    }
    // Update the complete data for this node  
    else {
      // Recover data from the previous node. We could also save this in a hidden form field.
      if (!isset($node->complete['runs'])) $node->complete['runs'] = $old_config->complete['runs'];
      if (!isset($node->complete['nid'])) $node->complete['nid'] = $old_config->complete['nid'];
      
      _complete_update_action($node->complete);
    } 
  }
  elseif (complete_enabled_node($old_config)) {
    // The complete data for this node has been removed, remove also from the database.
    _complete_delete($node);
  }
}

/**
 * Delete the complete action for this node.
 */
function _complete_delete(&$node) {
  db_query("DELETE FROM {complete} WHERE nid = %d", $node->nid);
}

/**
 * Insert complete action.
 */
function _complete_insert_action(&$action) {
  _complete_build_action($action);
  drupal_write_record('complete', $action);
}

/**
 * Update complete action.
 */
function _complete_update_action(&$action) {
  _complete_build_action($action);
  drupal_write_record('complete', $action, 'nid');
}

/**
 *  Implement hook_views_api().
 */
function complete_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'complete') .'/views',
  );
}

/**
 * Theme function for the content complete block.
 */
function complete_theme() {
  return array(
    'complete_percent' => array(
      'arguments' => array(
        'complete_data' => NULL 
      ) 
    ) 
  );
}

/**
 * Block Theme function that displays the default output of a
 * complete percent. 
 * 
 * TODO integrate with views
 */
function theme_complete_percent($complete_data) { 
  drupal_add_css(drupal_get_path('module', 'complete') .'/complete.css', 'module');

  if ($complete_data['percent'] <= 33){
		$complete_data['class'] = 'red';
	}
	if ($complete_data['percent'] > 33){
		$complete_data['class'] = 'orange';
	}
	if ($complete_data['percent'] > 66){
		$complete_data['class'] = 'green';
	}
    
  $id = '';

  $output .= '<div id="complete-wrapper' .$id. '" class="complete-wrapper">';
  $output .= '<div class="complete-text-percent">'.t('!complete% complete', array(
    '!complete' => $complete_data['percent'] 
  )).'</div>';
  $output .= '<div class="complete-percent-bar-wrapper">';
  $output .= '<div class="complete-percent-bar '. $complete_data['class'] .'" style="width: '. $complete_data['percent'] .'%;"></div>';
  $output .= '</div>';
  
  $output .= '<div class="complete-text">';
  $output .= '<span class="complete-text-nextaction">';
  if ($complete_data['nextaction'] && $complete_data['nextpercent']) {
    $output .= t('!nextaction for !nextpercent%.', array(
      '!nextaction' => l($complete_data['nextaction'], $complete_data['nextactionpath']),
      '!nextpercent' => $complete_data['nextpercent'],
    ));
  }
  $output .= '</span>';
  if ($complete_data['complete_page_path']) {
	  $output .= '<span class="complete-text-nextpage">';
	  $output .= ' '.l($complete_data['complete_page_title'] .' &raquo;', $complete_data['complete_page_path'], array('html' => TRUE));
	  $output .= '</span>';
  }
  
  $output .= '</div>';

  $output .= '</div>';
  
  return $output;
}
