<?php

/** 
 *@file 
 *Administration page callbacks for the complete module. 
 */

/**
 * Admin settings form
 */
function complete_admin_settings() {
	$form['complete_page_path'] = array(
		'#title' => t('Page path'),
		'#type' => 'textfield',
		'#description' => t('Provide the path to the full page provided by views here. Example: <code>complete</code> (default view).'),
		'#default_value' => variable_get('complete_page_path', 'complete'), 
	);
	
	$form['complete_page_title'] = array(
		'#title' => t('Page link title'),
		'#type' => 'textfield',
		'#description' => t('Provide the name of the link to the full page here. Example: <code>See all</code>'),
		'#default_value' => variable_get('complete_page_title', t('See all')), 
	);
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save') 
  );
  
  $form['#tree'] = TRUE; // return nested array of fields	

  return $form;
}

/**
 * Admin settings form submit
 */
function complete_admin_settings_submit($form, &$form_state) {	
  if (is_array($form_state['values']) && ! empty($form_state['values'])) {
		variable_set('complete_page_path', $form_state['values']['complete_page_path']); 
		variable_set('complete_page_title', $form_state['values']['complete_page_title']);

    drupal_set_message(t("Your settings have been saved."));
  }
}

/**
 * Reset counters form
 */
function complete_admin_reset_counters() {
  $form = array();

  // Warn them and give a button that shows we mean business
  $form['warning'] = array('#value' => t('<p><strong>Note:</strong> there is no confirmation. Be sure of your action before clicking the button.<br />You may want to make a backup of the database and/or the complete table prior to using this feature.</p>'));
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reset counters.'),
  );

  return $form;
}

/**
 * Reset counters form submit
 */
function complete_admin_reset_counters_submit($form, &$form_state) {
  if (is_array($form_state['values']) && ! empty($form_state['values'])) {
    complete_reset_counters();
    drupal_set_message(t("All complete counters have been reset."));
  }
}

