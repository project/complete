<?php

/**
 * Filter handler for filtering nodes by whether or not they're complete-enabled.
 */
class complete_handler_filter_complete_content_type extends views_handler_filter_boolean_operator {
  function query() {
    $types = node_get_types();
    $complete_types = array();
    foreach ($types as $type => $name) {
      if (complete_enabled_type($type)) {
        $complete_types[] = '"'. $type .'"';
      }
    }
    $this->query->add_where(0, 'node.type '. (empty($this->value) ? 'NOT IN' : 'IN') .'('. implode(', ', $complete_types) .')');
  }
}