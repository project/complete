<?php

/**
 *  @file
 *  This defines views handlers for the complete module.
 */

class complete_views_handler_field_description extends views_handler_field_custom {
  function render($values) {
		// get a list of rules
		$rules = rules_get_configured_items('rules');
		
		// get the rule name for this nid
		$result = db_query("SELECT rule FROM {complete} WHERE nid = %d", $values->nid);
    $item = db_fetch_array($result);
    $name = $item['rule'];
		
		$label = $rules[$name]['#label'];
		return $label;
  }
}
