<?php

/**
 * @file
 *Provide the views Complete plugin.
 */

/**
  * Implementation of views_plugin_style().
  */
class complete_style_plugin extends views_plugin_style {
  function render($values) {
    // Make calculations here (always on the fly but using cached values to compute with...)
    
    foreach ($values as &$value) {
      complete_annotate_percentage($value);
    }
    
    
    $data = $this->compute_sum($values);
    
    if ($data['percent'] <= 33){
  		$data['class'] = 'red';
  	}
  	if ($data['percent'] > 33){
  		$data['class'] = 'orange';
  	}
  	if ($data['percent'] > 66){
  		$data['class'] = 'green';
  	}
    
    return theme('complete_percent', $data);
  }
  
   /**
    * Style validation.
    */
   function validate() {
     $errors = parent::validate();
     
     if (empty($this->display->display_options['style_plugin'])) {
       return $errors;
     }
     
     // Make sure a completeness field is provided.
     $fields = $this->display->handler->get_option('fields');
     
     // vsm
     
     return $errors;
   }
  
  /**
   * Compute the sum of a series of complete values.
   * TODO: include other values such as next field, path, ...
   * 
   * @return 
   *   An array of fields such as percentage complete (summed).
   */
  function compute_sum(&$rows) {
    $data['percent'] = 0;
    
    foreach ($rows as &$row) {
      $data['percent'] = ($row->complete_runs / $row->complete_total_runs) * 100;      
    }
    
    if (count($rows)) {
      $data['percent'] = floor($data['percent'] / count($rows));
      
    }
    
    return $data;
  }
  
  
}