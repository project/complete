<?php

/**
 *  @file
 *  This defines views hooks for the complete module. It will be loaded automatically as needed by the Views module.
 */

/**
 * Implementation of hook_views_data()
 */
function complete_views_data() {
  
  $data['complete']['table']['group'] = t('Complete Action');
  $data['complete']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );
  
  // Argument for filtering a specific feed
  $data['complete']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('The complete node'),
    'relationship' => array(
      'label' => t('Complete node'),
      'base' => 'node',
      'base field' => 'nid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  
  // Number of times this rule has been run
  $data['complete']['runs'] = array(
    'title' => t('Runs'),
    'help' => t('Number of times this rule has run'),
    'field' => array(
      'handler' => 'complete_views_handler_field_progress',
      'additional fields' => array('total_runs'),
      'click sortable' => TRUE
    ),
   'argument' => array(
     'handler' => 'views_handler_argument_numeric',
     'numeric' => TRUE,
     'name field' => 'runs',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // Number of times this rule has to be run
  $data['complete']['total_runs'] = array(
   'title' => t('Total runs'),
   'help' => t('Number of times this rule will need to be run'),
   'field' => array(
     'handler' => 'views_handler_field_numeric',
     'click sortable' => TRUE
   ),
   'argument' => array(
     'handler' => 'views_handler_argument_numeric',
     'numeric' => TRUE,
     'name field' => 'runs',
   ),
     'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
  );
  
  // Rule completed?
  $data['complete']['completed'] = array(
   // 'real field' => 'runs',
   'title' => t('Completed?'),
   'help' => t("Is the rule completed?"),
   'field' => array(
     'handler' => 'views_handler_field_boolean',
     'click sortable' => TRUE,
   ),
   'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
    ),
   'sort' => array(
        'handler' => 'views_handler_sort',
    ),
  );
  
  // Remaining times the rule needs to be run
  $data['complete']['remaining'] = array(
   // 'real field' => 'runs',
   'title' => t('Remaining runs'),
   'help' => t("Remaining times a rule needs to be run before completion."),
   'field' => array(
     'handler' => 'views_handler_field_numeric',
     'click sortable' => TRUE,
   ),
   'argument' => array(
     'handler' => 'views_handler_argument_numeric',
     'numeric' => TRUE,
     'name field' => 'runs',
   ),
   'filter' => array(
        'handler' => 'views_handler_filter_numeric',
    ),
   'sort' => array(
        'handler' => 'views_handler_sort',
    ),
  );
  
  $data['complete']['complete_content_type'] = array(
    'title' => t('Complete content type'),
    'help' => t('Whether or not the content type is used as complete action.'),
    'filter' => array(
      'handler' => 'complete_handler_filter_complete_content_type',
      'label' => t('Is complete content type'),
    ),
  );
  
  // description taken from the rule, another non database field
  $data['complete']['description'] = array(
   'title' => t('Description'),
   'help' => t("Description of the rule (provided by Rules)"),
   'field' => array(
     'handler' => 'complete_views_handler_field_description',
     'click sortable' => TRUE,
   ),
   'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
  );
  
  $data['complete']['path'] = array(
      'title' => t('Path'),
      'help' => t('The path at which the rule can be completed'),
      'field' => array(
        'handler' => 'views_handler_field_url',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    );
  
  $data['complete']['rule'] = array(
    'title' => t('Rule'),
    'help' => t('The rule name (provided by Rules)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
    return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function complete_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'complete') . '/views/handlers',
    ),
    'handlers' => array(      
      // field handlers
      'complete_views_handler_field_description' => array(
        'parent' => 'views_handler_field_custom',
      ),
      'complete_views_handler_field_progress' => array(
        'parent' => 'views_handler_field_custom',
      ),
      // filter handlers
      'complete_handler_filter_complete_content_type' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
    ),
  );
}

/**
  * Implementation of hook_views_plugin().
  */
function complete_views_plugins() {
  return array(
    'module' => 'complete',
    'style' => array(
      'complete' => array(
        'path' => drupal_get_path('module', 'complete') . '/views/plugins',
        'title' => t('Complete'),
        'help' => t('Display completeness for a number of rows.'),
        'handler' => 'complete_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}


